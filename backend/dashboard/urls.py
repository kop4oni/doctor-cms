from django.urls import path

from .views import dashboard, MainPage

urlpatterns = [
    path("", MainPage.as_view(), name="main"),
    path("dashboard", dashboard, name="dashboard"),
]