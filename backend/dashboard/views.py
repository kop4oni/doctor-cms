from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import RedirectView


class MainPage(LoginRequiredMixin, RedirectView):
    url = reverse_lazy("dashboard")


def dashboard(request):
    return render(request, 'dashboard/index.html')
