from .forms import AuthForm, RegisterForm, ProfileForm
from .models import Profile
from django.contrib.auth import login, logout
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import FormView


class Login(FormView):
    form_class = AuthForm
    template_name = 'account/form.html'
    extra_context = {
        'form_name': 'Login',
    }
    success_url = reverse_lazy("dashboard")

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user=user)

        return super().form_valid(form)


class Register(FormView):
    form_class = RegisterForm
    template_name = 'account/form.html'
    extra_context = {
        'form_name': "Registration",
    }
    success_url = reverse_lazy("dashboard")

    def form_valid(self, form):
        form.save()

        return super().form_valid(form)


def logout_page(request):
    user = request.user

    if user:
        logout(request)

    return redirect("/")

def profile_page(request):
    Profile.objects.get_or_create(user=request.user)
    profile = Profile.objects.get(user=request.user)
    context = {
        "profile": profile,
    }
    return render(request, 'account/profile.html', context)


def profile_page_edit(request):
    Profile.objects.get_or_create(user=request.user)
    profile = Profile.objects.get(user=request.user)

    if request.method == "POST":
        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
    else:
        form = ProfileForm(instance=profile)
    context = {
        "form": form,
    }
    return render(request, 'account/profile_edit.html', context)

